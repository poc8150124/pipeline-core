node('slave') {
    @Library('pipeline-library') _
    String projectName = JOB_NAME.split('-')[0]
    String environmentName = deployment_stat.getEnvironmentName()
	  Map projectDetail = deployment_stat.getProjectDetail(projectName)
	  Map dimension = deployment_stat.createDimension(projectName, environmentName, projectDetail.language, projectDetail.projectType)
    long timeBuildDuration = System.currentTimeMillis()
    long timeStartStageDuration = System.currentTimeMillis()
    String stageName = ''
    def records = []
    def measureName = deployment_stat.measureName

    def createMeasureStage = { ->
        long timeEndStageDuration = System.currentTimeMillis()
        records = deployment_stat.createMeasure(dimension, records, measureName.STAGE_BUILD_DURATION, stageName, timeEndStageDuration, timeEndStageDuration - timeStartStageDuration)
        timeStartStageDuration = timeEndStageDuration
    }
    Map configures = [:]
    
    List<String> serviceNames = []
    int maxParallel = 5
    List groupOfParallel = []
    try {
        timeout(time: 30, unit: 'MINUTES') {
            stageName = 'CheckOut'
            stage(stageName){
                cleanWs()
                configures = airflow.init(projectName)
                airflow.gitCheckout(configures)
                createMeasureStage()
            }
            stageName = 'UnitTest'
            stage(stageName) {
                airflow.unitTest(configures)
                createMeasureStage()
            }
            stageName = 'SonarQube Analysis'
            stage(stageName) {
                configures = sonar_scanner.init(projectName, configures)
                sonar_scanner.projectTypeScanner(projectName, configures)
                sonar_scanner.checkQualityGateByCondition(configures)
                createMeasureStage()
            }
            stageName = 'Remove Old Workspace & Clean Build'
            stage(stageName) {
                cleanWs()
                airflow.gitCheckout(configures)
                createMeasureStage()
            }
            stageName = 'Component Tests By Postman'
            stage(stageName) {
                airflow.regressionTest(configures)
                createMeasureStage()
            }
            stageName = 'Component Tests By Cypress'
            stage(stageName) {
                if (configures['cypress_regressionTest_Enable']  && configures.cypress_regressionTest_Enable.toBoolean() == true) {
                    cypress_component_test.cypressIntegrationTestingEslint(configures)
                    environmentName = environmentName.replace('alpha-', '')
                    cypressConfigures = cypress_report.init(projectName, environmentName)
                    cypressIsSuccess = cypress_report.cypressIntegrationTesting(cypressConfigures)
                    cypress_report.putFileToS3AndWorkchat(cypressConfigures, cypressIsSuccess, configures['projectGitBranch'])
                    if (!cypressIsSuccess) {
                        error('Component Tests By Cypress is failed')
                    }
                }
                createMeasureStage()
            }
            stageName = 'Build & Push Docker image migrations'
            stage(stageName) {
                airflow.addGenerateProjectConfigScriptToProject(configures)
                airflow.prepareDocker(configures)
                airflow.pushDockerSubImageToECR(configures)
                airflow.putLifeCyclePolicy(configures)
                // airflow.registerLogGroups()
                // airflow.registerNewJobDef(configures)
                // createMeasureStage()
            }
            stageName = 'Run migrations'
            stage(stageName) {
                // airflow.submitJobAndWait(configures)
                // createMeasureStage()
            }
            stageName = 'Build & Push Docker image main'
            stage(stageName) {
                airflow.pushDockerImage(configures)
                airflow.putLifeCyclePolicy(configures)
                createMeasureStage()
            }
            stageName = 'Re-Deploy & Check State'
            stage(stageName) {
                timeout(time: 30, unit: 'MINUTES') {
                serviceNames = airflow.getServiceNames(configures)
                Map taskRePushImages = [:]
                for (serviceName in serviceNames) {
                    if (serviceName.contains('webserver')) {
                    airflow.updateAirflowServices(configures['ecsClusterName'], serviceName, configures)
                    airflow.checkServiceIsSteadyState(configures['ecsClusterName'], serviceName)
                    }
                }
                parallelServicesGroups = serviceNames.collate(maxParallel as int)
                for (List parallelServicesGroup in parallelServicesGroups) {
                    parallelServicesGroup.each {
                                    serviceName ->
                    taskRePushImages["${serviceName}: Re-Deploy & Healthcheck"] = {
                        if (!serviceName.contains('webserver')) {
                        airflow.updateAirflowServices(configures['ecsClusterName'], serviceName, configures)
                        airflow.checkServiceIsSteadyState(configures['ecsClusterName'], serviceName)
                        }
                    }
                    }
                    parallel taskRePushImages
                }
                }
            }
            stageName = 'Upload File To S3 & Triger Data Sync'
            stage(stageName) {
                // airflow.uploadFileToS3(configures['airflow_source_paths'].split(','), configures['airflow_target_path'])
                // airflow.trigerDataSync(projectName, environmentName)
                // createMeasureStage()
            }
        }
    } catch (Exception ex) {
        long currentTimeDuration = System.currentTimeMillis()
		records = deployment_stat.createMeasure(dimension, records, measureName.STAGE_ERROR, stageName, currentTimeDuration, 1)
		records = deployment_stat.createMeasure(dimension, records, measureName.ERROR, '', currentTimeDuration, 1)
		status.Error()
		println(ex)
		currentBuild.result = 'FAILURE'
    } finally {
        cleanWs()
        long currentTimeDuration = System.currentTimeMillis()
        records = deployment_stat.createMeasure(dimension, records, measureName.BUILD_DURATION, '', currentTimeDuration, currentTimeDuration - timeBuildDuration)
        deployment_stat.postDeploymentStat(records)
        utility.removeUnusedDockerImages()
    }
}